# US - 001: Agendamento de apresentação para clientes Premium.

**Sendo** um Agente de Vendas 

**Posso** de forma automática manter a minha agenda atualizada 

**Para** apresentação corporativa aos novos Clientes Premium com isso melhorar o relacionamento com o mesmo.


## Critérios de Aceite



### 01 - Evento criado para Clientes Premium com faturamento acima de $1.000.000,00

**Dado** que um Cliente (Account) com categoria (Category__c) "Premium" <br>
**E** Receita Anual acima de 1.000.000 (um milhão)

**Quando** ele for inserido, ou seja, for criado.

**Então** Um novo Evento deverá ser criado com os seguintes dados:

* Descrição (`Description`) : 'Apresentar o portifolio de Produtos para o novo cliente com enfoque em nossa carteira Premium'
* Assunto (`Subject`) : 'Apresentação instucional dos produtos Premium'
* Data e Hora de Início (`StartDateTime`) : Agendar para 1 dia após a data de criação do Cliente *¹
* Data e Hora de Fim (`EndDateTime`) : Deverá ter uma hora de duração
* Relativo a (`WhatId`) : Id do Cliente em questão.
* Tipo de Contato (`ContactType__c`) : 'Telefônico'


### 02 - Evento não criado para Cliente Premium com faturamento menor que $1.000.000,00

**Dado** que um Cliente (Account) com categoria (Category__c) "Premium" <br>
**E** Receita Anual de que $600.000,00

**Quando** ele for inserido, ou seja, for criado.

**Então** não deverá ser criado nenhum evento para este cliente.


## 03 - Evento não criado para Cliente Standard

**Dado**  um Cliente (Account) com categoria (Category__c) "Standard"

**Quando**  ele for inserido, ou seja, for criado.

**Então** não deverá ser criado nenhum evento para este cliente.


##  Campos necessários


Criar campo Category (`Category__c`) em Account do Tipo Lista de Opções (picklist) com os valores pedidos acima e outros para melhorar o cenário de teste.

Criar campo Contact Type (`ContactType__c`) em Atividade e deixa-lo somente visível para o Evento, ou seja, no layout de Evento.

<br>

## Pontos Obrigatórios

* Criar repositório privado ( nome-topi-run-us001 ) e adicionar o "eduardocarvalho@brq.com, oseas.tormen@topi.io, rafael.colatusso@topi.io" como colaboradores

* Todas as classes com 100% de cobertura.

* Utilização do Pattern TriggerHandler, temos que ver a Triggger e a chamada para TriggerHandler, acesse o repositorio https://github.com/kevinohara80/sfdc-trigger-framework, para mais informações




<br>

## Direcionar o projeto para seu repositório do github

git remote set-url origin https://github.com/seuuser/nomedorepo.git


## Dica

Este repositório acompanha uma implementação muito interessante de TriggerHandler, recomendamos que a mesma seja utilizada, para mais informações sobre esta implemetação consulte o [github](https://github.com/kevinohara80/sfdc-trigger-framework)